import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Math.exp;
import static java.lang.Math.log;
import static java.lang.Math.pow;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class VariablesAleatoriasUniforme {
    public static void main(String args[]) throws FileNotFoundException, IOException {
        //Cantidad de numeros a generar
        Scanner s = new Scanner(System.in);
        //Thread t = new Thread();
        //Variables iniciales (Multiplicador, Modulo, Semilla, Constante Aditiva)
        //float a = 77, modl = 19735, x, c = 77, res;
        float a = 77, modl = 197, x, c = 77, res;
        //variables para distribucion uniforme
        int Vminimo= 1;
        int Vmaximo=6;
        
        ArrayList<Float> resultados = new ArrayList<>();
        System.out.print("Teclee el tamaño de muestra que tiene: ");
        int nn = s.nextInt();

        String ruta = "C:\\MuestraServicioCarga";
        if (new File(ruta).exists()) {
            FileReader llegadas = new FileReader(ruta);
            try (BufferedReader br = new BufferedReader(llegadas)) {
                for (int i = 1; i < nn; i++) {
                    String datos = br.readLine();
                    //Separacion de las variables
                    StringTokenizer st = new StringTokenizer(datos);
                    while (st.hasMoreTokens()) {
                        Float s2 = Float.valueOf(st.nextToken());
                        x = s2;
                        res = (a * x + c) % modl;
                        x = res;
                        res = res / modl;
                        //System.out.println(i + ". " + " " + res);
                        resultados.add(res);
                        float Xres=Vminimo+res*(Vmaximo-Vminimo);
                        float unif=Xres;
                        System.out.println(unif);
                    }
                }
            }
        }
    }
}