

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Math.log;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class VariablesAleatoriasExponencial {
    public static void main(String args[]) throws FileNotFoundException, IOException {
        //Cantidad de numeros a generar
        Scanner s = new Scanner(System.in);
        float a = 77, modl = 197, x, c = 77, res;
        //variables para distribucion exponencial
        double lambda=0.5;
        
        ArrayList<Float> resultados = new ArrayList<>();
        System.out.print("Teclee el tamaño de muestra que tiene: ");
        int nn = s.nextInt();

        String ruta = "C:\\Users\\Casa\\Documents\\Azeneth\\ITS\\6TO SEMESTRE\\MODELADO DE SISTEMAS DINAMICOS\\Tarea5-PruebaGeneradoresDeNumeros\\prueba\\Modelado\\src\\Actividad5\\MuestraCarga";
        if (new File(ruta).exists()) {
            FileReader llegadas = new FileReader(ruta);
            try (BufferedReader br = new BufferedReader(llegadas)) {
                for (int i = 1; i < nn; i++) {
                    String datos = br.readLine();
                    //Separacion de las variables
                    StringTokenizer st = new StringTokenizer(datos);
                    while (st.hasMoreTokens()) {
                        Float s2 = Float.valueOf(st.nextToken());
                        x = s2;
                        res = (a * x + c) % modl;
                        x = res;
                        res = res / modl;
                        //System.out.println(i + ". " + " " + res);
                        resultados.add(res);
                        float expo=(float) ((-log(res))/(lambda));
                        System.out.println(expo);
                        
                    }
                }
            }
        }
    }
}
