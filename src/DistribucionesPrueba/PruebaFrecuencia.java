package ProyectoFinal;

/**
 *
 * @author EmanuelGarciaMT
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class PruebaFrecuencia {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        float rmcl;
        //Celdas
        int c1 = 0, c2 = 0, c3 = 0, c4 = 0, c5 = 0, c6 = 0, c7 = 0, c8 = 0, c9 = 0, c10 = 0;
        Scanner s = new Scanner(System.in);
        System.out.print("Teclee el tamaño de muestra que tiene: ");
        int nn = s.nextInt();

        String ruta = "C:\\Users\\EmanuelGarciaMT\\Desktop\\ProyectoFinalSimulacion\\ProyectoFinalModelado\\ResultadoLlegadasMCL.txt";
        //String ruta = "C:\\Users\\EmanuelGarciaMT\\Desktop\\ProyectoFinalSimulacion\\ProyectoFinalModelado\\ResultadoCargasMCL.txt";
        if (new File(ruta).exists()) {
            FileReader llegadas = new FileReader(ruta);
            try (BufferedReader br = new BufferedReader(llegadas)) {
                for (int i = 1; i < nn; i++) {
                    String datos = br.readLine();
                    //Separacion de las variables
                    StringTokenizer st = new StringTokenizer(datos);
                    while (st.hasMoreTokens()) {
                        Float s2 = Float.valueOf(st.nextToken());
                        rmcl = s2;
                        //REALIZAR HISTOGRAMA DE FRECUENCIAS
                        if ((rmcl >= 0) && (rmcl <= 0.1)) {
                            c1++;
                        } else if ((rmcl >= 0.11) && (rmcl <= 0.2)) {
                            c2++;
                        } else if ((rmcl >= 0.21) && (rmcl <= 0.3)) {
                            c3++;
                        } else if ((rmcl >= 0.31) && (rmcl <= 0.4)) {
                            c4++;
                        } else if ((rmcl >= 0.41) && (rmcl <= 0.5)) {
                            c5++;
                        } else if ((rmcl >= 0.51) && (rmcl <= 0.6)) {
                            c6++;
                        } else if ((rmcl >= 0.61) && (rmcl <= 0.7)) {
                            c7++;
                        } else if ((rmcl >= 0.71) && (rmcl <= 0.8)) {
                            c8++;
                        } else if ((rmcl >= 0.81) && (rmcl <= 0.9)) {
                            c9++;
                        } else {
                            c10++;
                        }
                    }
                }
                System.out.println("\n**HISTOGRAMA DE FRECUENCIAS**");
                System.out.println("Rango1 (0-0.1)   : " + c1);
                System.out.println("Rango2 (0.11-0.2): " + c2);
                System.out.println("Rango3 (0.21-0.3): " + c3);
                System.out.println("Rango4 (0.31-0.4): " + c4);
                System.out.println("Rango5 (0.41-0.5): " + c5);
                System.out.println("Rango6 (0.51-0.6): " + c6);
                System.out.println("Rango7 (0.61-0.7): " + c7);
                System.out.println("Rango8 (0.71-0.8): " + c8);
                System.out.println("Rango9 (0.81-0.9): " + c9);
                System.out.println("Rango10(0.91-1)  : " + c10);
                //CALCULAR X2
                int mr = 10;
                float VE = nn / mr;
                float x2cal = 0;
                int[] arreglof = {c1, c2, c3, c4, c5, c6, c7, c8, c9, c10};

                for (int j = 0; j < arreglof.length; j++) {
                    x2cal = x2cal + (((arreglof[j] - VE) * (arreglof[j] - VE)) / VE);
                }
                System.out.println("x2 calculada:" + x2cal);

                //PRUEBA
                if (x2cal <= 16.9190) {
                    System.out.println("**Los numeros probablemente son aleatorios**");
                } else {
                    System.out.println("**No son aleatorios**");
                }

            }
        }
    }
}
