package ProyectoFinal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author EmanuelGarciaMT
 */
public class DistribucionLogNormal {

    public static void main(String args[]) throws FileNotFoundException, IOException {
        //Cantidad de numeros a generar
        Scanner s = new Scanner(System.in);
        float a = 77, modl = 197, x, c = 77, res;

        float xmed = 0, media = 0, xdesviacion = 0, desviacion = 0, xplus;

        double lognormal = 0;
        ArrayList<Float> resultados = new ArrayList<>();

        System.out.print("Teclee el tamaño de muestra que tiene la Muestra de lavadoras: ");
        int lav = s.nextInt();

        System.out.print("Teclee el tamaño de muestra que tiene la Muestra de secadoras: ");
        int sec = s.nextInt();

        String rutalavadora = "C:\\Users\\EmanuelGarciaMT\\Desktop\\ProyectoFinalSimulacion\\ProyectoFinalModelado\\MuestraServicioLavadora.txt";
        if (new File(rutalavadora).exists()) {
            FileReader usolavadora = new FileReader(rutalavadora);
            BufferedReader br = new BufferedReader(usolavadora);
            for (int i = 1; i < lav; i++) {
                String datos = br.readLine();
                //Separacion de las variables
                StringTokenizer st = new StringTokenizer(datos);
                while (st.hasMoreTokens()) {
                    Float s2 = Float.valueOf(st.nextToken());
                    x = s2;
                    res = (a * x + c) % modl;
                    x = res;
                    res = res / modl;
                    //System.out.println(i + ". " + " " + res);
                    resultados.add(res);

                    //Media
                    xmed = xmed + res;

                    //Desviacion
                    xdesviacion = (float) (xdesviacion + (Math.abs(Math.pow(res - xmed, 2))));

                    //System.out.println("x: "+res);
                    //System.out.println("suma media: "+xmed);
                }
            }
            media = xmed / lav;
            desviacion = (float) Math.sqrt(xdesviacion / lav);
            //System.out.println(xmed);
            //System.out.println(desviacion);
            
            System.out.println("Lavadora: ");
            for (int j = 1; j < lav; j++) {
                xplus = resultados.get(j - 1);
                //System.out.println(xplus);
                lognormal = Math.exp(-Math.pow((Math.log(xplus) - media), 2) / 2 * Math.pow(desviacion, 2)) / (media * desviacion * Math.sqrt(2 * Math.PI));
                System.out.println(" " + lognormal);
            }
        }
        
        xmed=0;
        media=0;
        xdesviacion=0;
        desviacion=0;
        xplus=0;
        String rutasecadora = "C:\\Users\\EmanuelGarciaMT\\Desktop\\ProyectoFinalSimulacion\\ProyectoFinalModelado\\MuestraServicioSecadora.txt";
        if (new File(rutasecadora).exists()) {
            FileReader usosecadora = new FileReader(rutasecadora);
            BufferedReader br = new BufferedReader(usosecadora);
            for (int i = 1; i < sec; i++) {
                String datos = br.readLine();
                //Separacion de las variables
                StringTokenizer st = new StringTokenizer(datos);
                while (st.hasMoreTokens()) {
                    Float s2 = Float.valueOf(st.nextToken());
                    x = s2;
                    res = (a * x + c) % modl;
                    x = res;
                    res = res / modl;
                    //System.out.println(i + ". " + " " + res);
                    resultados.add(res);

                    //Media
                    xmed = xmed + res;

                    //Desviacion
                    xdesviacion = (float) (xdesviacion + (Math.abs(Math.pow(res - xmed, 2))));

                    //System.out.println("x: "+res);
                    //System.out.println("suma media: "+xmed);
                }
            }
            media = xmed / sec;
            desviacion = (float) Math.sqrt(xdesviacion / sec);
            //System.out.println(xmed);
            //System.out.println(desviacion);
            System.out.println("Secadora: ");
            for (int j = 1; j < sec; j++) {
                xplus = resultados.get(j - 1);
                //System.out.println(xplus);
                lognormal = Math.exp(-Math.pow((Math.log(xplus) - media), 2) / 2 * Math.pow(desviacion, 2)) / (media * desviacion * Math.sqrt(2 * Math.PI));
                System.out.println(" " + lognormal);
            }
        }
        
    }
}
