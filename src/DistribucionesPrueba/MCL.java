package ProyectoFinal;

/**
 *
 * @author EmanuelGarciaMT
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class MCL {

    public static void main(String args[]) throws FileNotFoundException, IOException {
        //Cantidad de numeros a generar
        Scanner s = new Scanner(System.in);
        //Thread t = new Thread();
        //Variables iniciales (Multiplicador, Modulo, Semilla, Constante Aditiva)
        //float a = 77, modl = 19735, x, c = 77, res;
        float a = 77, modl = 197, x, c = 77, res;
        
        ArrayList<Float> resultados = new ArrayList<>();
        System.out.print("Teclee el tamaño de muestra que tiene: ");
        int nn = s.nextInt();

        //String ruta = "C:\\Users\\EmanuelGarciaMT\\Desktop\\ProyectoFinalSimulacion\\ProyectoFinalModelado\\MuestraLlegada.txt";
        String ruta = "C:\\Users\\EmanuelGarciaMT\\Desktop\\ProyectoFinalSimulacion\\ProyectoFinalModelado\\MuestraCarga.txt";
        if (new File(ruta).exists()) {
            FileReader llegadas = new FileReader(ruta);
            try (BufferedReader br = new BufferedReader(llegadas)) {
                for (int i = 1; i < nn; i++) {
                    String datos = br.readLine();
                    //Separacion de las variables
                    StringTokenizer st = new StringTokenizer(datos);
                    while (st.hasMoreTokens()) {
                        Float s2 = Float.valueOf(st.nextToken());
                        x = s2;
                        res = (a * x + c) % modl;
                        x = res;
                        res = res / modl;
                        System.out.println(i + ". " + " " + res);
                        resultados.add(res);
                    }
                }
                //System.out.println(resultados);
                //String rutapruebabondad = "C:\\Users\\EmanuelGarciaMT\\Desktop\\ProyectoFinalSimulacion\\ProyectoFinalModelado\\ResultadoLlegadasMCL.txt";
                String rutapruebabondad = "C:\\Users\\EmanuelGarciaMT\\Desktop\\ProyectoFinalSimulacion\\ProyectoFinalModelado\\ResultadoCargasMCL.txt";
                File pruebaBondad = new File(rutapruebabondad);
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(pruebaBondad))) {
                    for (int j = 1; j < nn; j++) {
                        bw.write("" + resultados.get(j-1));
                        bw.newLine();
                    }
                    System.out.println("El archivo para la prueba de bondad ha sido creado");
                }
            }
        }
    }
}