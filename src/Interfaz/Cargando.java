package Interfaz;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

public class Cargando extends javax.swing.JFrame {

    /**
     * Creates new form Cargando
     */
    Timer t=null;
    private int count= 0;
    public Cargando() {
        initComponents();
        setExtendedState(MAXIMIZED_BOTH);//Maximiza la pantalla
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        int height = pantalla.height;
        int width = pantalla.width;
        setSize(width / 2, height / 2);
        
        setVisible(true);
        setLocationRelativeTo(null);;
        Barra.setStringPainted(true);
        UIManager.put("ProgressBar.background", Color.yellow);

        t = new Timer(200, (ActionEvent e) -> {
            count++;
            Barra.setValue(count);
            if (Barra.getValue() < 10) {
                Barra.setValue(Barra.getValue() + 1);
            }
            if (Barra.getValue() == 10) {
                dispose();
            }
        });
        t.start();
        new Simulador().setVisible(false);
    }
    
        @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        Barra = new javax.swing.JProgressBar();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1197, 582));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(407, 582));
        setSize(new java.awt.Dimension(1198, 582));
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 36)); // NOI18N
        jLabel1.setText("Simulador procesando resultados, espere un momento...");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(240, 10, 1010, 50);

        Barra.setBackground(javax.swing.UIManager.getDefaults().getColor("InternalFrame.activeTitleBackground"));
        Barra.setForeground(new java.awt.Color(51, 0, 51));
        Barra.setMaximum(10);
        Barra.setBorderPainted(false);
        Barra.setOpaque(true);
        getContentPane().add(Barra);
        Barra.setBounds(330, 80, 610, 30);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Animacion.gif"))); // NOI18N
        jLabel2.setText("jLabel2");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(40, 150, 1180, 490);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Azul.jpg"))); // NOI18N
        getContentPane().add(jLabel3);
        jLabel3.setBounds(0, 0, 1400, 810);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Cargando.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Cargando().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JProgressBar Barra;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    // End of variables declaration//GEN-END:variables
}
