package Interfaz;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import static java.lang.Math.log;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class Simulador extends javax.swing.JFrame {

    /**
     * Creates new form Simulador
     */
    public Simulador() {
        initComponents();
        setExtendedState(MAXIMIZED_BOTH);//Maximiza la pantalla
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        int height = pantalla.height;
        int width = pantalla.width;
        setSize(width / 2, height / 2);
        
        setVisible(true);
        setLocationRelativeTo(null);;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        BotonSalir = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablaCambio = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        TiempoLimite = new javax.swing.JTextField();
        BotonInicio = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TablaResultadosFinales = new javax.swing.JTable();
        BotonGuardar = new javax.swing.JButton();
        BotonReinicio = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);
        setBackground(new java.awt.Color(0, 153, 153));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setFocusable(false);
        setForeground(new java.awt.Color(0, 204, 204));
        setMaximizedBounds(new java.awt.Rectangle(0, 0, 1250, 725));
        setUndecorated(true);
        setResizable(false);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
                formWindowGainedFocus(evt);
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
            }
        });
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Century Gothic", 3, 36)); // NOI18N
        jLabel1.setText("Minutos");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(1050, 30, 200, 40);

        BotonSalir.setBackground(new java.awt.Color(51, 153, 255));
        BotonSalir.setFont(new java.awt.Font("Yu Gothic UI Semibold", 1, 24)); // NOI18N
        BotonSalir.setText("Salir del Simulador");
        BotonSalir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BotonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonSalirActionPerformed(evt);
            }
        });
        getContentPane().add(BotonSalir);
        BotonSalir.setBounds(950, 560, 287, 109);

        TablaCambio.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Reloj", "Delta", "NC", "ColaLav1", "NCLA1", "ColaLav2", "NCLA2", "ColaLav3", "NCLA3", "ColaSec1", "NCSA1", "ColaSec2", "NCSA2", "ColaSec3", "NCSA3", "ColaEmpa", "NCEA"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Double.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TablaCambio.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        TablaCambio.setGridColor(new java.awt.Color(204, 204, 255));
        TablaCambio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TablaCambioMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TablaCambio);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(0, 100, 1250, 280);

        jLabel3.setBackground(new java.awt.Color(204, 204, 255));
        jLabel3.setFont(new java.awt.Font("Yu Gothic UI Semibold", 2, 36)); // NOI18N
        jLabel3.setText("SIMULACION LAVANDERIA");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 20, 500, 43);

        TiempoLimite.setFont(new java.awt.Font("Century Gothic", 3, 36)); // NOI18N
        TiempoLimite.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TiempoLimiteActionPerformed(evt);
            }
        });
        TiempoLimite.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TiempoLimiteKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TiempoLimiteKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TiempoLimiteKeyTyped(evt);
            }
        });
        getContentPane().add(TiempoLimite);
        TiempoLimite.setBounds(820, 20, 220, 50);

        BotonInicio.setBackground(new java.awt.Color(0, 204, 51));
        BotonInicio.setFont(new java.awt.Font("Yu Gothic UI Semibold", 1, 24)); // NOI18N
        BotonInicio.setText("Iniciar");
        BotonInicio.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BotonInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonInicioActionPerformed(evt);
            }
        });
        getContentPane().add(BotonInicio);
        BotonInicio.setBounds(50, 560, 163, 109);

        jLabel4.setFont(new java.awt.Font("Century Gothic", 3, 36)); // NOI18N
        jLabel4.setText("Tiempo limite:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(560, 30, 260, 40);

        TablaResultadosFinales.setBackground(new java.awt.Color(0, 204, 204));
        TablaResultadosFinales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TEML1", "TETL1", "TOL1", "TEML2", "TETL2", "TOL2", "TEML3", "TETL3", "TOL3", "TEMS1", "TETS1", "TOS1", "TEMS2", "TETS2", "TOS2", "TEMS3", "TETS3", "TOS3", "TEME", "TETE", "TOE"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TablaResultadosFinales.setGridColor(new java.awt.Color(204, 204, 255));
        TablaResultadosFinales.setSelectionBackground(new java.awt.Color(204, 204, 255));
        jScrollPane2.setViewportView(TablaResultadosFinales);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(10, 420, 1220, 90);

        BotonGuardar.setBackground(new java.awt.Color(255, 255, 0));
        BotonGuardar.setFont(new java.awt.Font("Yu Gothic UI Semibold", 1, 24)); // NOI18N
        BotonGuardar.setText("Guardar Registro");
        BotonGuardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BotonGuardar.setEnabled(false);
        BotonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(BotonGuardar);
        BotonGuardar.setBounds(640, 560, 250, 109);

        BotonReinicio.setBackground(new java.awt.Color(255, 102, 153));
        BotonReinicio.setFont(new java.awt.Font("Yu Gothic UI Semibold", 1, 24)); // NOI18N
        BotonReinicio.setText("Reiniciar");
        BotonReinicio.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BotonReinicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonReinicioActionPerformed(evt);
            }
        });
        getContentPane().add(BotonReinicio);
        BotonReinicio.setBounds(350, 560, 163, 109);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Azul.jpg"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 1250, 740);

        pack();
    }// </editor-fold>//GEN-END:initComponents

 float TLL = 0;//Llegada Cliente
    float NC = 0;//Numero Carga
    double TSlav[] = new double[6];//Las 3 estaciones de servicio Lavadoras
    double TSsec[] = new double[6];//Las 3 estaciones de servicio Secadoras
    double TsEmpa = 5; //Estacion de servicio Empacado
    
    int ColaLavadora[] = new int[6];
    int ColaSecadora[] = new int[6];
    
    int NCLA[] = new int[6];// Numero Cargas Lavadora Atendidas
    int NCSA[] = new int[6];// Numero Cargas Secadoras Atendidas
    
    double TETL[] = new double[6];// Tiempo Estimado Total Lavadora
    double TETS[] = new double[6];// Tiempo Estimado Total Secadora
    
    double TOL[] = new double[6];// Tiempo Ocio Lavadora
    double TOS[] = new double[6];// Tiempo Ocio Secadora
    
    double TEML[] = new double[6];// Tiempo Estimado Promedio Lavadora
    double TEMS[] = new double[6];// Tiempo Estimado Promedio Secadora
    
    int ColaEmpacado = 0;
    
    int NCEA = 0;// Numero Cargas Empacado Atendidas
    
    double TETE = 0;// Tiempo Estimado Total Empacado
    
    double TOE = 0;// Tiempo Ocio Empacado
    
    double TEME = 0;// Tiempo Estimado Promedio Empacado
    
    double delta;
    
    double reloj = 0;
    
    boolean corre = true; //Finaliza el simulador
    boolean cliente = true; //Controla la llegada del cliente

    float tl;//Tiempo Limite
    
    boolean prioridad;
    double cp = 0;// Carga prioritaria
    
    //Parametros MCL
    float a = 77, modl = 1993557111, c = 77, res = 0;
    double x = Math.floor(Math.random() * 100 + 1);

    //Parametros Exponencial
    float expo;
    double lambda = 0.5;
    
    //Parametros Uniforme
    int Vminimo = 1;
    int Vmaximo = 6;
    float Xres, unif;
    
    //Parametros LogNormal
    float xmed = 0, media = 0, xdesviacion = 0, desviacion = 0, xplus;
    double lognormal = 0;

    public void MCL() {
        res = (float) ((a * x + c) % modl);
        x = res;
        res = res / modl;
    }

    public void Exponencial() {
        MCL();
        expo = (float) ((-log(res)) / (lambda));
        TLL = expo;
        System.out.println("TLL "+TLL);
    }

    public void Uniforme() {
        MCL();
        Xres = Vminimo + res * (Vmaximo - Vminimo);
        unif = Xres;
        NC = unif;
    }
    
    public void LogNormalLavadora(){
        double medialav=1869.375;
        double desviacionlav=201.66944368478;
        
        double varianzalav=Math.pow(desviacionlav,2);
        
        double Mediaylav=Math.pow(Math.log(medialav), -.5)* Math.log((varianzalav/Math.pow(medialav,2))+1);
        
        double Varylav= Math.log((varianzalav/Math.pow(medialav,2))+1);
        
        double Sigmaylav=Math.sqrt(Varylav);
        
        for (int i = 1; i <= 3; i++) {
            for (int j=1;j<=12;j++){
                MCL();
                xplus = xplus+res;
                //System.out.println("xplus "+xplus);
            }
            
            lognormal=Math.exp(Mediaylav + Sigmaylav* (xplus-6));
            TSlav[i] = lognormal;
            //System.out.println("TiempoServicioLavadora "+TSlav[i]);
            xplus=0;
        }
    }
    
    public void LogNormalSecadora() {
        MCL();
        double mediasec = 2030.34375;
        double desviacionsec = 184.99;

        double varianzasec = Math.pow(desviacionsec, 2);

        double Mediaysec = Math.pow(Math.log(mediasec), -.5) * Math.log((varianzasec / Math.pow(mediasec, 2)) + 1);

        double Varysec = Math.log((varianzasec / Math.pow(mediasec, 2)) + 1);

        double Sigmaysec = Math.sqrt(Varysec);

        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 12; j++) {
                MCL();
                xplus = xplus + res;
                //System.out.println("xplus "+xplus);
            }

            lognormal = Math.exp(Mediaysec + Sigmaysec * (xplus - 6));
            TSsec[i] = lognormal;
            //System.out.println(TSsec[i]);
            xplus=0;
        }
    }
    
    boolean DiaInicio=true;
    DecimalFormat decimales=new DecimalFormat("#0.0000");
    public void SimulacionFinal() throws InterruptedException {
       // JOptionPane.showMessageDialog(null,"Da click en Aceptar y comenzara la simulacion");
        tl = Float.parseFloat(TiempoLimite.getText());
        while (corre == true) {
            Exponencial();
            Uniforme();
            LogNormalLavadora();
            LogNormalSecadora();

            /*System.out.println(expo);
            System.out.println(unif);
            System.out.println(lognormal);
            System.out.println("--------------------------------------");*/
            delta = expo;

            double minimo = Math.min(TLL, Math.min(TSlav[1], Math.min(TSlav[2], Math.min(TSlav[3], Math.min(TSsec[1], Math.min(TSsec[2], Math.min(TSsec[3], TsEmpa)))))));
            if (minimo > 0) {
                delta = minimo;
                decimales.format(delta);
            }

            /*System.out.println(delta);
            System.out.println(TLL);
            System.out.println(NC);
            System.out.println(TSlav[1]);
            System.out.println(TSlav[2]);
            System.out.println(TSlav[3]);
            System.out.println(TSsec[1]);
            System.out.println(TSsec[2]);
            System.out.println(TSsec[3]);
            System.out.println("--------------------------------------");*/
            reloj = reloj + delta;
            decimales.format(reloj);
            
            String data2 = null;
            if (delta == TLL) {
                data2 = "Cliente";
            } else if (delta == NC) {
                data2 = "Carga Ropa";
            } else if (delta == TSlav[1]) {
                data2 = "Lavadora 1";
            } else if (delta == TSlav[2]) {
                data2 = "Lavadora 2";
            } else if (delta == TSlav[3]) {
                data2 = "Lavadora 3";
            } else if (delta == TSsec[1]) {
                data2 = "Secadora 1";
            } else if (delta == TSsec[2]) {
                data2 = "Secadora 2";
            } else if (delta == TSsec[3]) {
                data2 = "Secadora 3";
            } else if (delta == TsEmpa) {
                data2 = "Empacado";
            }
            
            System.out.println("---------------------");
            System.out.println("Datos");
            System.out.println("Delta "+delta);
            System.out.println(TETL[1]);
            System.out.println(TETL[2]);
            System.out.println(TETL[3]);

            System.out.println(TETS[1]);
            System.out.println(TETS[2]);
            System.out.println(TETS[3]);
            System.out.println(ColaSecadora[0]);
            System.out.println(ColaSecadora[1]);
            System.out.println(ColaSecadora[2]);

            System.out.println(TETE);

            System.out.println("\n\n\n\n");
            
            //System.out.println("reloj " + reloj);
            //Tiempos Totales de las estaciones de servicio
            TETL[1] = TETL[1] + ColaLavadora[1] * delta;
            TETL[2] = TETL[2] + ColaLavadora[2] * delta;
            TETL[3] = TETL[3] + ColaLavadora[3] * delta;

            TETS[1] = TETS[1] + ColaSecadora[1] * delta;
            TETS[2] = TETS[2] + ColaSecadora[2] * delta;
            TETS[3] = TETS[3] + ColaSecadora[3] * delta;

            TETE = TETE + ColaEmpacado * delta;
            

            
            
            
            while (cliente == true) {
                TLL = (float) (TLL - delta);
                if (TLL == 0) {
                    MCL();
                    if (res < .3) {// si res es menor a .3, la carga es prioritarizada  
                        prioridad = true;
                        Uniforme();
                        cp = NC;
                    } else {
                        prioridad = false;
                    }
                    if (ColaLavadora[2] > ColaLavadora[1] || ColaLavadora[3] > ColaLavadora[1]) {
                        
                        ColaLavadora[1]++;
                        
                    } else if (ColaLavadora[1] > ColaLavadora[3] || ColaLavadora[2] > ColaLavadora[3]) {
                        
                        ColaLavadora[3]++;
                        
                    } else if (ColaLavadora[1] > ColaLavadora[2] || ColaLavadora[3] > ColaLavadora[2]) {
                        
                        ColaLavadora[2]++;
                        
                    } else {
                        MCL();
                        if (res > .6) {
                            ColaLavadora[1]++;
                        } else if (res < .6 || res > .9) {
                            ColaLavadora[2]++;
                        } else {
                            ColaLavadora[3]++;
                        }
                    }
                    Exponencial();
                    Uniforme();
                } else {
                    cliente = false;
                }
            }
            cliente = true;

            //Lavadoras
            for (int i = 0; i <= 3; i++) {
                TSlav[i] = TSlav[i] - delta;
                decimales.format(TSlav[i]);
                //System.out.println("Lavadora " + i + " " + TSlav[i]);
                if (prioridad == true) {
                    if (ColaLavadora[i] > 0) {
                        NCLA[i]++;
                        ColaLavadora[i]--;
                        ColaSecadora[i]++;
                        System.out.println("La lavadora se utilizo para depositar carga prioritaria");
                        cp = 0;
                        Uniforme();
                        prioridad = false;
                    }
                } else {
                    if (TSlav[i] < 0) {
                        TOL[i] = TOL[i] - TSlav[i];
                        decimales.format(TOL[i]);
                        TSlav[i] = 0;
                        if (TSlav[i] == 0) {
                            NCLA[i]++;
                            if (ColaSecadora[2] > ColaSecadora[1] || ColaSecadora[3] > ColaSecadora[1]) {
                                ColaSecadora[1]++;
                            } else if (ColaSecadora[1] > ColaSecadora[3] || ColaSecadora[2] > ColaSecadora[3]) {
                                ColaSecadora[3]++;
                            } else if (ColaSecadora[1] > ColaSecadora[2] || ColaSecadora[3] > ColaSecadora[2]) {
                                ColaSecadora[2]++;
                            } else {
                                MCL();
                                if (res > .6) {
                                    ColaSecadora[3]++;
                                } else if (res < .6 || res > .9) {
                                    ColaSecadora[2]++;
                                } else {
                                    ColaSecadora[1]++;
                                }
                            }
                            if (ColaLavadora[i] > 0) {
                                ColaLavadora[i]--;
                                LogNormalLavadora();
                            }
                        }
                    }
                }
            }

            //System.out.println("---------------------------");

            //Secadoras
            for (int i = 0; i <= 3; i++) {
                TSsec[i] = TSsec[i] - delta;
                decimales.format(TSsec[i]);
                //System.out.println("Secadora " + i + " " + TSsec[i]);

                if (TSsec[i] < 0) {
                    TOS[i] = TOS[i] - TSsec[i];
                    decimales.format(TOS[i]);
                    TSsec[i] = 0;
                    if (TSsec[i] == 0) {
                        NCSA[i]++;
                        ColaEmpacado++;
                        if (ColaSecadora[i] > 0) {
                            ColaSecadora[i]--;
                            LogNormalSecadora();
                        }
                    }
                }
            }

            //Empacado
            TsEmpa = TsEmpa - delta;
            decimales.format(TsEmpa);
            if (TsEmpa < 0) {
                TOE = TOE - TsEmpa;
                TsEmpa = 0;
                if (TsEmpa == 0) {
                    NCEA++;
                    if (ColaEmpacado > 0) {
                        ColaEmpacado=ColaEmpacado-2;
                        if(ColaEmpacado<0){
                            ColaEmpacado++;
                        }
                        TsEmpa=2;
                    }
                }
            }
            //Impresion de datos 
            Object[] row3 = {decimales.format(reloj), data2, NC, ColaLavadora[1], NCLA[1], ColaLavadora[2], NCLA[2], 
                ColaLavadora[3], NCLA[3], ColaSecadora[1], NCSA[1], ColaSecadora[2], NCSA[2], ColaSecadora[3], NCSA[3], ColaEmpacado, NCEA};
            DefaultTableModel model = (DefaultTableModel) TablaCambio.getModel();
            model.addRow(row3);

            if (reloj >= tl) {
                corre = false;
                for (int i = 0; i <= 3; i++) {
                    TEML[i] = TETL[i] / tl;
                    decimales.format(TEML[i]);
                    TEMS[i] = TETS[i] / tl;
                    decimales.format(TEMS[i]);
                }
                TEME = TETE / tl;
                Object[] row2 = {decimales.format(TEML[1]), decimales.format(TETL[1]), decimales.format(TOL[1]), decimales.format(TEML[2]), decimales.format(TETL[2]),
                    decimales.format(TOL[2]), decimales.format(TEML[3]), decimales.format(TETL[3]), decimales.format(TOL[3]), decimales.format(TEMS[1]), 
                    decimales.format(TETS[1]), decimales.format(TOS[1]), decimales.format(TEMS[2]), decimales.format(TETS[2]), decimales.format(TOS[2]), 
                    decimales.format(TEMS[3]), decimales.format(TETS[3]), decimales.format(TOS[3]), decimales.format(TEME), decimales.format(TETE), decimales.format(TOE)};
                DefaultTableModel model2 = (DefaultTableModel) TablaResultadosFinales.getModel();
                model2.addRow(row2);
                BotonGuardar.setEnabled(true);
            }
        }
        
    }



    
    
    private void BotonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonSalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_BotonSalirActionPerformed

    private void TiempoLimiteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TiempoLimiteActionPerformed

    }//GEN-LAST:event_TiempoLimiteActionPerformed

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked

    }//GEN-LAST:event_formMouseClicked

    private void formWindowGainedFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowGainedFocus

    }//GEN-LAST:event_formWindowGainedFocus

    private void TablaCambioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaCambioMouseClicked

    }//GEN-LAST:event_TablaCambioMouseClicked

    private void BotonInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonInicioActionPerformed
        try {
            //El reloj inicializa
            SimulacionFinal();
            //BotonInicio.setEnabled(false);
            //TiempoLimite.setEnabled(false);
        } catch (InterruptedException ex) {
            Logger.getLogger(Simulador.class.getName()).log(Level.SEVERE, null, ex);
        }
        new Cargando().setVisible(true);

    }//GEN-LAST:event_BotonInicioActionPerformed

    private void TiempoLimiteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TiempoLimiteKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TiempoLimiteKeyPressed

    private void TiempoLimiteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TiempoLimiteKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_TiempoLimiteKeyReleased

    private void TiempoLimiteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TiempoLimiteKeyTyped
        //Descarta toda captura y asegura el Enter
        char cTeclaPresionada=evt.getKeyChar();
        //Da click al boton al detectar aneter
        if(cTeclaPresionada==KeyEvent.VK_ENTER){
            BotonInicio.doClick();
        }

        //Evita que tecle solo numeros y un punto 
        if (((cTeclaPresionada < '0') || (cTeclaPresionada > '9')) && (cTeclaPresionada != KeyEvent.VK_BACK_SPACE) && (cTeclaPresionada != '.')) {
            /* lo que deseo colocar aqui es si ya se pulso el caracter (.) el mismo no se pueda repetir*/
            evt.consume();
        }
        
    }//GEN-LAST:event_TiempoLimiteKeyTyped

    private void BotonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonGuardarActionPerformed
        //Registro estados
        try{
            Thread t=new Thread(){
                @Override
                public void run(){
                    XSSFWorkbook workbook=new XSSFWorkbook();
                    XSSFSheet hoja=workbook.createSheet();
                    
                    XSSFRow fila=hoja.createRow(0);
                    fila.createCell(0).setCellValue("RELOJ");
                    fila.createCell(1).setCellValue("DELTA");
                    fila.createCell(2).setCellValue("NC");
                    fila.createCell(3).setCellValue("COLALAV1");
                    fila.createCell(4).setCellValue("NCLA1");
                    fila.createCell(5).setCellValue("COLALAV2");
                    fila.createCell(6).setCellValue("NCLA2");
                    fila.createCell(7).setCellValue("COLALAV3");
                    fila.createCell(8).setCellValue("NCLA3");
                    fila.createCell(9).setCellValue("COLASEC1");
                    fila.createCell(10).setCellValue("NCSA1");
                    fila.createCell(11).setCellValue("COLASEC2");
                    fila.createCell(12).setCellValue("NCSA2");
                    fila.createCell(13).setCellValue("COLASEC3");
                    fila.createCell(14).setCellValue("NCSA3");
                    fila.createCell(15).setCellValue("COLAEMPA");
                    fila.createCell(16).setCellValue("NCEA");
                    
                    XSSFRow filas;
                    Rectangle rec;
                    for(int i=0;i<TablaCambio.getRowCount();i++){
                        rec=TablaCambio.getCellRect(i, 0, true);
                        try{
                            TablaCambio.scrollRectToVisible(rec);
                        }catch(java.lang.ClassCastException e){
                            
                        }
                        TablaCambio.setRowSelectionInterval(i,i);
                        filas=hoja.createRow((i+1));
                        filas.createCell(0).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        
                        filas.createCell(1).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(2).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(3).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(4).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(5).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(6).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(7).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(8).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(9).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(10).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(11).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(12).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(13).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(14).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(15).setCellValue(TablaCambio.getValueAt(i,0).toString());
                        filas.createCell(16).setCellValue(TablaCambio.getValueAt(i,0).toString());
                    }
                    try{
                        workbook.write(new FileOutputStream(new File("./RegistroEstado.xlsx")));
                        Desktop.getDesktop().open(new File("./Prueba.xlsx"));
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(Simulador.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Simulador.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            };
            t.start();
        }catch(Exception e){
            
        }
        
        //Registro Tiempos finales
        try {
            Thread t2 = new Thread() {
                @Override
                public void run() {
                    XSSFWorkbook workbook2 = new XSSFWorkbook();
                    XSSFSheet hoja2 = workbook2.createSheet();

                    XSSFRow fila2 = hoja2.createRow(0);
                    fila2.createCell(0).setCellValue("TEML1");
                    fila2.createCell(1).setCellValue("TETL1");
                    fila2.createCell(2).setCellValue("TOL1");
                    fila2.createCell(3).setCellValue("TEML2");
                    fila2.createCell(4).setCellValue("TETL2");
                    fila2.createCell(5).setCellValue("TOL2");
                    fila2.createCell(6).setCellValue("TEML3");
                    fila2.createCell(7).setCellValue("TETL3");
                    fila2.createCell(8).setCellValue("TOL3");
                    fila2.createCell(9).setCellValue("TEMS1");
                    fila2.createCell(10).setCellValue("TETS1");
                    fila2.createCell(11).setCellValue("TOS1");
                    fila2.createCell(12).setCellValue("TEMS2");
                    fila2.createCell(13).setCellValue("TETS2");
                    fila2.createCell(14).setCellValue("TOS2");
                    fila2.createCell(15).setCellValue("TEMS3");
                    fila2.createCell(16).setCellValue("TETS3");
                    fila2.createCell(17).setCellValue("TOS3");
                    fila2.createCell(18).setCellValue("TEME");
                    fila2.createCell(19).setCellValue("TETE");
                    fila2.createCell(20).setCellValue("TOE");

                    XSSFRow filas2;
                    Rectangle rec2;
                    for (int i = 0; i < TablaResultadosFinales.getRowCount(); i++) {
                        rec2 = TablaResultadosFinales.getCellRect(i, 0, true);
                        try {
                            TablaResultadosFinales.scrollRectToVisible(rec2);
                        } catch (java.lang.ClassCastException e) {

                        }
                        TablaResultadosFinales.setRowSelectionInterval(i, i);
                        filas2 = hoja2.createRow((i + 1));
                        filas2.createCell(0).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());

                        filas2.createCell(1).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(2).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(3).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(4).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(5).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(6).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(7).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(8).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(9).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(10).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(11).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(12).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(13).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(14).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(15).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(16).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(17).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(18).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(19).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                        filas2.createCell(20).setCellValue(TablaResultadosFinales.getValueAt(i, 0).toString());
                    }
                    try {
                        workbook2.write(new FileOutputStream(new File("./RegistroTiemposFinales.xlsx")));
                        Desktop.getDesktop().open(new File("./Prueba.xlsx"));
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(Simulador.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Simulador.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            };
            t2.start();
            JOptionPane.showMessageDialog(null,"Los registros se han guardado exitosamente en la carpeta raiz de la computadora, espere a que se carguen los datos");
        } catch (HeadlessException e) {

        }
    }//GEN-LAST:event_BotonGuardarActionPerformed

    private void BotonReinicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonReinicioActionPerformed
        Simulador.main(null);
        dispose();
    }//GEN-LAST:event_BotonReinicioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Simulador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Simulador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Simulador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Simulador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Simulador().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton BotonGuardar;
    public javax.swing.JButton BotonInicio;
    public javax.swing.JButton BotonReinicio;
    private javax.swing.JButton BotonSalir;
    private javax.swing.JTable TablaCambio;
    public javax.swing.JTable TablaResultadosFinales;
    public javax.swing.JTextField TiempoLimite;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables

}
