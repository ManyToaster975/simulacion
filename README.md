# PIA Modelado y Simulacion
Emanuel Garcia
---

# Simulacion Lavanderia
Tiene como finalidad demostrar el estatus de la lavanderia de acuerdo a la cantidad de tiempo (en minutos) que se trabaja en el negocio.
Para ver la ejecucion mas a detalle descargar Simulador.jar 

---

## Significado de las variables
- Reloj: Es el tiempo transcurrido en minutos
- Delta: Especie de cambio que ha ocurrido en el momento
- NC: Numero de cargas (canastos llevados)
- ColaLav#: Es la fila de espera de cada lavadora antes de entrar
- NCLA#: Es el numero de cargas antendidas en su respectiva lavadora.
- ColaEmpa: Fila de espera para la fase final
- NCEA: Numero de cargas empacadas atendidas.
- TEML#: Tiempo ejecucion media de la lavadora
- TETL#: Tiempo ejecucion total de la lavadora
- TOL#: Tiempo de ocio total de la lavadora
- TEMS#: Tiempo ejecucion media de la secadora
- TETS#: Tiempo ejecucion total de la secadora
- TOS#: Tiempo de ocio de la secadora
- TEME: Tiempo ejecucion media del empacado
- TETE: Tiempo ejecucion total del empacado
- TOE: Tiempo de ocio del empacado

---

## Acciones
- Iniciar: Inicia el simulador 
- Reiniciar: Reinicia el simulador
- Guardar Registro: Almacen el registro del sistema para futuros analisis en un Excel (.xls)
- Salir: Sale del sistema

## Imagenes

![Imgur](https://i.imgur.com/bf1wE7D.png)

---

![Imgur](https://i.imgur.com/j8XfcD9.png)

---

![Imgur](https://i.imgur.com/lrc0rY7.png)

---